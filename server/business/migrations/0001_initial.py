# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-11-24 13:47
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Budget',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='BudgetStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
            ],
        ),
        migrations.CreateModel(
            name='CurrencyCodes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=3, unique=True)),
                ('name', models.CharField(max_length=40, unique=True)),
                ('symbol', models.CharField(max_length=1)),
                ('dollar_exchange_rate', models.IntegerField(default=1)),
            ],
        ),
        migrations.CreateModel(
            name='Money',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('money', models.FloatField(default=0)),
                ('currency_code', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='business.CurrencyCodes')),
            ],
        ),
        migrations.CreateModel(
            name='SafeMoney',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', models.CharField(blank=True, max_length=250, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('money', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='business.Money')),
            ],
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=40)),
                ('comment', models.CharField(blank=True, max_length=250, null=True)),
                ('type_transaction', models.BooleanField()),
                ('created_at', models.DateTimeField()),
                ('money', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='business.Money')),
            ],
        ),
        migrations.CreateModel(
            name='Type',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=40)),
                ('type', models.BooleanField()),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(blank=True, max_length=25, null=True)),
                ('last_name', models.CharField(blank=True, max_length=25, null=True)),
                ('age', models.IntegerField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('budget', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='business.Budget')),
                ('currency_code', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='business.CurrencyCodes')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='transaction',
            name='type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='business.Type'),
        ),
        migrations.AddField(
            model_name='transaction',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='safemoney',
            name='type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='business.Type'),
        ),
        migrations.AddField(
            model_name='safemoney',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='budgetstatus',
            name='budget',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='business.Money'),
        ),
        migrations.AddField(
            model_name='budgetstatus',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='budget',
            name='money',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='business.Money'),
        ),
        migrations.AddField(
            model_name='budget',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='type',
            unique_together=set([('user', 'title', 'type')]),
        ),
        migrations.AlterUniqueTogether(
            name='safemoney',
            unique_together=set([('user', 'type')]),
        ),
        migrations.AlterUniqueTogether(
            name='budgetstatus',
            unique_together=set([('user', 'date')]),
        ),
        migrations.AlterUniqueTogether(
            name='budget',
            unique_together=set([('owner', 'money')]),
        ),
    ]
