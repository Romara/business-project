from __future__ import unicode_literals

from django.db import migrations

from ..models import CurrencyCodes


CURRENCY_CODES = [
    {
        'title': 'USD',
        'name': 'UNITED_STATES_DOLLARS',
        'symbol': u'$',
    },
    {
        'title': 'EUR',
        'name': 'EUROS',
        'symbol': u'€',
    },
    {
        'title': 'GBP',
        'name': 'BRITISH_POUNDS',
        'symbol': u'£',
    },
    {
        'title': 'CAD',
        'name': 'CANADIAN_DOLLARS',
        'symbol': u'$',
    },
    {
        'title': 'JPY',
        'name': 'JAPANESE_YENS',
        'symbol': u'¥',
    },
    {
        'title': 'CNY',
        'name': 'CHINESE_RENMINBI_YUANS',
        'symbol': u'¥',
    },
    {
        'title': 'INR',
        'name': 'INDIAN_RUPEES',
        'symbol': u'₹',
    },
    {
        'title': 'SGD',
        'name': 'SINGAPORE_DOLLARS',
        'symbol': u'$',
    },
    {
        'title': 'BYN',
        'name': 'BELARUS_RUBLES',
        'symbol': u'B',
    },
]


def add_currency_codes(apps, schema_editor):
    for currency_code in CURRENCY_CODES:
        currency_code_object = CurrencyCodes()
        currency_code_object.title = currency_code['title']
        currency_code_object.name = currency_code['name']
        currency_code_object.symbol = currency_code['symbol']
        currency_code_object.save()


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(add_currency_codes),
    ]
