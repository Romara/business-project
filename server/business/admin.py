from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register(CurrencyCodes)
admin.site.register(Type)
admin.site.register(Money)
admin.site.register(Budget)
admin.site.register(UserSettings)
admin.site.register(Transaction)
admin.site.register(SafeMoney)
admin.site.register(BudgetStatus)
