# Model for
from datetime import timedelta, datetime

from django.db import models
# Django user model
from django.contrib.auth.models import User


class CurrencyCodes(models.Model):
    title = models.CharField(max_length=3, unique=True)
    name = models.CharField(max_length=40, unique=True)
    symbol = models.CharField(max_length=1)

    dollar_exchange_rate = models.FloatField(default=1)

    def save(self, *args, **kwargs):
        if self.dollar_exchange_rate == 0:
            raise ZeroDivisionError('Dollar exchange rate can not be 0!')
        return super(CurrencyCodes, self).save(*args, **kwargs)

    def __str__(self):
        return self.title


class Type(models.Model):
    user = models.ForeignKey(User)

    title = models.CharField(max_length=40)

    type = models.BooleanField()

    def __str__(self):
        return self.title

    class Meta:
        unique_together = ("user", "title", "type")


class Money(models.Model):
    money = models.FloatField(default=0)
    currency_code = models.ForeignKey(CurrencyCodes)

    def __add__(self, other):
        count_of_money = other.in_currency_code(self.currency_code.title)
        self.money += count_of_money
        return self

    def __sub__(self, other):
        count_of_money = other.in_currency_code(self.currency_code.title)
        self.money -= count_of_money
        return self

    def __str__(self):
        return str(self.money) + self.currency_code.symbol

    def in_currency_code(self, currency_title):
        if self.currency_code.title == currency_title:
            return self.money
        # Be careful with CurrencyCodes
        # Here maybe exception CurrencyCodes.DoesNotExist
        dollar_exchange_rate = CurrencyCodes.objects.get(title=currency_title).dollar_exchange_rate
        dollars = self.money / self.currency_code.dollar_exchange_rate
        return dollars * dollar_exchange_rate

    def change_currency_code(self, currency_code_title):
        # Here maybe exception CurrencyCodes.DoesNotExist
        currency_code_safe = CurrencyCodes.objects.get(title=currency_code_title)
        # To dollar first
        money = self.money / self.currency_code.dollar_exchange_rate
        # To currency code
        money = money * currency_code_safe.dollar_exchange_rate

        self.money = money
        self.currency_code = currency_code_safe

    def save(self, currency_code_title=None, *args, **kwargs):
        if currency_code_title:
            currency_code = CurrencyCodes.objects.get(title=currency_code_title)
            self.currency_code = currency_code
        super(Money, self).save(*args, **kwargs)


class Budget(models.Model):
    owner = models.ForeignKey(User)
    money = models.ForeignKey(Money)

    def __str__(self):
        return str(self.money)

    class Meta:
        unique_together = ("owner", "money")


class UserSettings(models.Model):
    user = models.ForeignKey(User)

    # Info about user
    first_name = models.CharField(max_length=25, null=True, blank=True)
    last_name = models.CharField(max_length=25, null=True, blank=True)
    age = models.IntegerField(null=True, blank=True)

    # Main budget
    budget = models.ForeignKey(Budget)

    currency_code = models.ForeignKey(CurrencyCodes)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def change_currency_code(self, currency_code_title):
        currency_code = CurrencyCodes.objects.get(title=currency_code_title)
        self.currency_code = currency_code
        self.budget.money.save(currency_code_title)
        self.save()

    def __str__(self):
        return self.user.username


class Transaction(models.Model):
    user = models.ForeignKey(User)

    title = models.CharField(max_length=40)
    comment = models.CharField(max_length=250, null=True, blank=True)

    money = models.ForeignKey(Money)

    # Income or Outcome
    type_transaction = models.BooleanField()

    type = models.ForeignKey(Type)

    created_at = models.DateTimeField()

    def save(self, money_count=None, currency_code_title=None, *args, **kwargs):
        if money_count and currency_code_title:
            money = Money()
            money.money = money_count
            money.save(currency_code_title=currency_code_title)
            self.money = money

        super(Transaction, self).save(*args, **kwargs)

        money_status = Money()
        money_status.currency_code = self.money.currency_code
        money_status.money = self.money.money
        money_status.save()
        BudgetStatus.objects.update_or_create(user=self.user,
                                              date=str(self.created_at)[:10],
                                              defaults={'budget': money_status})

        self._update_all_budget_statuses_after_date()

        user_settings = UserSettings.objects.get(user=self.user)
        money = user_settings.budget.money
        if self.type_transaction:
            money += self.money
        else:
            money -= self.money
        money.save()

    def _update_all_budget_statuses_after_date(self):
        budget_statuses = BudgetStatus.objects.filter(user=self.user)
        for budget_status in budget_statuses:
            sum_day = self._get_budget_on_current_day(str(budget_status.date)[:10])
            if sum_day is not None:
                budget_status.budget = sum_day
                budget_status.save()
            else:
                budget_status.delete()

    def _get_budget_on_current_day(self, date):
        this_date = datetime.strptime(date, '%Y-%m-%d')
        if not Transaction.objects.filter(user=self.user, created_at__year=this_date.year,
                                          created_at__month=this_date.month,
                                          created_at__day=this_date.day):
            return None
        transactions = Transaction.objects.filter(user=self.user, created_at__lte=str(date)[:10])
        all_money = Money()
        all_money.money = 0
        all_money.currency_code = self.money.currency_code

        for transaction in transactions:
            if transaction.type_transaction:
                all_money += transaction.money
            else:
                all_money -= transaction.money
        all_money.save()
        return all_money

    def delete(self, using=None, keep_parents=False):
        budget_money = Budget.objects.get(owner=self.user).money

        if self.type_transaction:
            budget_money -= self.money
        else:
            budget_money += self.money
        budget_money.save()

        super(Transaction, self).delete(using, keep_parents)

        self._update_all_budget_statuses_after_date()

    def __str__(self):
        if self.type_transaction:
            return '+' + str(self.money) + ' from ' + self.title
        else:
            return '-' + str(self.money) + ' from ' + self.title


class SafeMoney(models.Model):
    user = models.ForeignKey(User)

    type = models.ForeignKey(Type)
    comment = models.CharField(max_length=250, null=True, blank=True)

    money = models.ForeignKey(Money)

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.money) + ' on ' + str(self.type)

    def save(self, *args, **kwargs):
        super(SafeMoney, self).save(*args, **kwargs)

    class Meta:
        unique_together = ("user", "type")


class BudgetStatus(models.Model):
    user = models.ForeignKey(User)
    budget = models.ForeignKey(Money)

    date = models.DateField()

    def __str__(self):
        return 'User has budget ' + str(self.budget) + ' at ' + str(self.date) + '.'

    class Meta:
        unique_together = ("user", "date")
