PlanningApp.factory('PieChart', function($http) {
    var service = {};

    var PlanCanvas = document.getElementById("safe-money-chart").getContext('2d');
    var Dataset = [];
    var Labels = [];
    var PlanChart = [];
    var Colors = [];
    
    var pieChartsOptions = {
        legend: {
            display: true,
            labels: {fontSize: 16}
        },
        tooltips: {
            bodyFontSize: 16,
            xPadding: 13,
            yPadding: 13,
            callbacks: {
                label: function(tooltipItem, data) {
                    var allData = data.datasets[tooltipItem.datasetIndex].data;
                    var tooltipLabel = data.labels[tooltipItem.index];
                    var tooltipData = allData[tooltipItem.index];
                    var total = 0;
                    for (var i in allData) {
                        total += allData[i];
                    }
                    var tooltipPercentage = Math.round((tooltipData / total) * 100);
                    return ' ' + tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
                }
            }
        }
    };

    $http.get('/api/safe-money/')
        .success(function(data) {
            for (var i = data.length - 1; i >= 0; i--) {
                Dataset.unshift(data[i].money.money);
                Labels.unshift(data[i].type.title);
            }

            Colors = randomColor({luminosity: 'light', count: data.length});
        
            if (data.length === 0) {
                $('#plans-no-data').removeClass('none-display');
                $('#safe-money-chart').addClass('none-display');
            } else {
                PlanChart = new Chart(PlanCanvas, {
                    type: 'pie',
                    data: {
                        labels: Labels,
                        datasets: [{
                            backgroundColor: Colors,
                            data: Dataset
                        }]
                    },
                    options: pieChartsOptions
                });
            }
        })
        .error(function(error) {
            console.log(error);
        });

    service.addValue = function (value, label) {
        $('#plans-no-data').addClass('none-display');
        $('#safe-money-chart').removeClass('none-display');
        var len = 0;
        if (PlanChart.data !== undefined) {
            len = PlanChart.data.labels.length;
            PlanChart.data.labels[len] = label;
            PlanChart.data.datasets[0].data[len] = value;
            PlanChart.data.datasets[0].backgroundColor[len] = randomColor({luminosity: 'light', count: 1})[0];
            PlanChart.update();
        } else {
            PlanChart = new Chart(PlanCanvas, {
                type: 'pie',
                data: {
                    labels: [label],
                    datasets: [{
                        backgroundColor: randomColor({luminosity: 'light', count: 1}),
                        data: [value]
                    }]
                },
                options: pieChartsOptions
            });
        }
    };

    service.deleteValue = function (label) {
        var index = PlanChart.data.labels.indexOf(label);
        PlanChart.data.labels.splice(index, 1);
        PlanChart.data.datasets[0].data.splice(index, 1);
        PlanChart.data.datasets[0].backgroundColor.splice(index, 1);
        PlanChart.update();
        if (PlanChart.data.datasets[0].data.length === 0) {
            $('#plans-no-data').removeClass('none-display');
            $('#safe-money-chart').addClass('none-display');            
        }
    };

    return service;
});
