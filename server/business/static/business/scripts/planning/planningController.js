PlanningApp.controller('PlanningController', function($scope, PlanningApiService, PieChart) {
    $scope.plans = [];
    $scope.today = moment().format("DD-MM-YYYY");
    $scope.total = 0;
    $scope.loader = true;
    $scope.selectedPlan = {};
    var api = PlanningApiService;
    var chart = PieChart;

    getPlans();  

    function getPlans() {
        api.getAllPlans().then(
            function (data) {
                data.forEach(function (e) {
                    $scope.plans.unshift({
                        'id': e.id,
                        'value': e.money.money,
                        'currency': PlanningApiService.currencyCode,
                        'category': e.type.title,
                    });
                });
                $scope.loader = false;
                $scope.total = countTotal();
                getTypes();
            },
            function (error) {
                console.log(error);
            }
        );
    }

    function getTypes() {
        api.getOutcomeTypes().then(
            function (data) {
                var currentCategories = $scope.plans.map(function(e) {
                    return e.category;
                });
                var avaiableCategories = data.filter(function(e) {
                    return ((e.type === false) && (currentCategories.indexOf(e.title) === -1));
                }).map(function(e) {
                    return '<option>' + e.title + '</option>';
                }).join("");
                $("#new-plan-field").html(avaiableCategories).selectpicker('refresh');
            },
            function (error) {
                console.log(error);
            }
        );
    }

    function countTotal() {
        if ($scope.plans.length === 0) {
            return 0;
        }
        return $scope.plans.map(function(e) {
            return e.value;
        }).reduce(function(sum, current) {
            return sum + current;
        }); 
    }

    $scope.addPlan = function (plan_form) {
        if (plan_form.$valid) {
            var category = $('#new-plan-field').find("option:selected").text();
            var value = $scope.planValue;
            api.postPlan(value, category).then(
                function(data) {
                    chart.addValue(value, category);
                    $scope.plans.unshift({
                        'id': data.id,
                        'value': data.money.money,
                        'currency': data.money.currency_code.title,
                        'category': data.type.title
                    });
                    getTypes();
                    $scope.total = countTotal();
                }, 
                function (error) {
                    console.log(error);
                }
            );
            $("#plan-form").slideToggle(500);
            $("#plan-form-show").slideToggle(500);
            $scope.planValue = "";
        }
    };

    $scope.selectPlan = function (index) {
        $scope.selectedPlan = angular.copy($scope.plans[index]);
    };

    $scope.deleteSelectedPlan = function () {
        var position = $scope.plans.map(function(e) { return e.id; }).indexOf($scope.selectedPlan.id);
        api.deletePlan($scope.selectedPlan.id).then(
            function () {
                $scope.plans.splice(position, 1);
                chart.deleteValue($scope.selectedPlan.category);
                getTypes();
                $scope.total = countTotal();
            }, 
            function (error) {
                console.log(error);
            }
        );
        console.log($scope.plans);
    };

});