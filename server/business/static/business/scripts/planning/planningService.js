PlanningApp.factory('PlanningApiService', function($http, $q) {
    var service = {};

    service.currencyCode = "USD";

    $http.get('/api/user-settings/').success(function(response) {
        service.currencyCode = response.currency_code.title;
    });


    /**
     * Making of GET request for all types
     */
    service.getOutcomeTypes = function () {
        var defer = $q.defer();
        $http.get('/api/types/')
            .success(function(response) {
                defer.resolve(response);
            })
            .error(function(error) {
                defer.reject(error);
            });
        return defer.promise;
    };

    /**
     * Making of GET request for all plans
     */
    service.getAllPlans = function () {
        var defer = $q.defer();
        $http.get('/api/safe-money/')
            .success(function(response) {
                defer.resolve(response);
            })
            .error(function(error) {
                defer.reject(error);
            });
        return defer.promise;
    };

    /**
     * Making of POST request with formated JSON data
     */
    service.postPlan = function (value, category) {
        var defer = $q.defer();
        var data = {
            "type": { 
                "title": category,
                "type": false
            }, 
            "money": { 
                "currency_code": { 
                    "title": service.currencyCode
                }, 
                "money": value
            }
        };

        $http.post('/api/safe-money/', data)
            .success(function(response) {
                defer.resolve(response);
            })
            .error(function(error) {
                defer.reject(error);
            });
        return defer.promise;
    };

    /**
     * Making of DELETE request for delete plan
     */
    service.deletePlan = function (planID) {
        var defer = $q.defer();
        var id = '?id=' + planID;
        $http.delete('/api/safe-money/' + id)
            .success(function(response) {
                defer.resolve(response);
            })
            .error(function(error) {
                defer.reject(error);
            });
        return defer.promise;
    }

    return service;
});