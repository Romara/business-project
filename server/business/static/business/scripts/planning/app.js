var PlanningApp = angular.module('PlanningApp', [])
                             .config(['$httpProvider', function($httpProvider){
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);


$("#plan-form-show").click(function(){
    $('.has-error').removeClass('has-error');
    $("#plan-form").slideToggle(500);
    $("#plan-form-show").slideToggle(500);
});

$("#cancel-plan").click(function(e){
    e.preventDefault();
    $("#plan-form").slideToggle(500);
    $("#plan-form-show").slideToggle(500);
});