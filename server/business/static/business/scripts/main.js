$(function(){

	/*login & registration tabs*/
	$('#registration-button').click(function(event) {
		$('#auth-tabs a:last').tab('show');
	});

	$('#registration-form').submit(function(e) {
		e.preventDefault();
    	$('#registration-form').css('display', 'none');
    	$('#registration-loader').css('display', 'block');
    	this.submit();
	});
    //change.bs.validator
	$('#registration-form').on('valid.bs.validator', function(e, data) {
    	formIsValid = true;
    	console.log(formIsValid);
    	$('.form-group.has-feedback', $(this)).each( function() {
    	    formIsValid = formIsValid && $(this).hasClass('has-success');
    	    console.log(formIsValid);
    	});
	
    	if(formIsValid) {
    	    $('.submit-button', $(this)).attr('disabled', false);
    	} else {
    	    $('.submit-button', $(this)).attr('disabled', true);
    	}
	});
	
	$('#sign-in-button').click(function(event) {
		$('#auth-tabs a:first').tab('show');
	});
	
	if (window.location.pathname === '/finances/') {
		$('li a[href="/finances"]').css('background-color', '#EEEEEE');
	} 
	else if (window.location.pathname === '/transactions/') {
		$('li a[href="/transactions"]').css('background-color', '#EEEEEE');
	}
    else if (window.location.pathname === '/planning/') {
        $('li a[href="/planning"]').css('background-color', '#EEEEEE');
    }
    else if (window.location.pathname === '/settings/') {
        $('li a[href="/settings"]').css('background-color', '#EEEEEE');
    }
	
	/*tooltips on*/
	$(document).ready(function(){
	    $('[data-toggle="tooltip"]').tooltip(); 
	});

    $(".round_inputs").on("change", function () {
        var input_values = parseFloat($(this).val());
        if (isNaN(input_values)) input_values = "";
        $(this).val(input_values.toFixed(2));
    });

});

