TransactionsApp.controller('TransactionsController', function($scope, TransactionsApiService) {
	$scope.today = moment().format("DD-MM-YYYY");    //Today date in dd-mm-yy format
	$scope.transactionType = false; //Negative/Positive transaction
	$scope.transactions = [];       //List of all transactions
	$scope.selectedTransaction = {};//Selected transaction for editing or deleting
	$scope.budget = {};
	$scope.incomeToday = 0;
	$scope.outcomeToday = 0;
	$scope.loader = true;

	var api = TransactionsApiService;

	//Get data from server on start
	getBudget();

	//Format dd-mm-yyyy as ISO date string
	function dateIsoFormat(date) {
		return date.split('-').reverse().join('-') + "T00:00:00Z";
	}

	//Function for counting income or outcome for today
	function transactionsSumToday (type) {
		var filteredByType = $scope.transactions.filter(function(e) {
			return ((e.type === type) && (e.date === $scope.today));
		});
		if (filteredByType.length !== 0) {
			return filteredByType.filter(function(e) {
				return (e.type === type) && (e.date === $scope.today);
			}).map(function(e) {
				if (!type) {
					return -e.value;
				}
				return e.value;
			}).reduce(function(sum, current) {
				return sum + current;
			});	
		}
		else {
			return 0;
		}
	}

	//Getting of all transactions from server when page opened
	function getTransactions() {
		api.getAllTransactions().then(
			function(data) {
				data.forEach(function (e) {
					$scope.transactions.unshift({
						'id': e.id,
						'date': moment(e.created_at).format("DD-MM-YYYY"),
   	     				'value': e.money.money,
   	     				'currency': /*e.money.currency_code.title*/$scope.budget.code,
   	     				'type': e.type_transaction,
   	     				'category': e.type.title,
   	     				'name': e.title
						});
				});
				$scope.incomeToday = transactionsSumToday(true);
				$scope.outcomeToday = transactionsSumToday(false);
				$scope.loader = false;
			},
			function (error) {
				console.log(error);
			}
		);
	}

	//Getting current budget value
	function getBudget() {
		api.getCurrentBudget().then(function (data) {
			$scope.budget.money = data.money;
			$scope.budget.code = data.currency_code;
			getTransactions();
		});
	}

	/**
 	 * Adds new transaction with data from transaction_form
 	 */
	$scope.addTransaction = function(transaction_form) {
		if(transaction_form.$valid) {
			var date = $('#tranaction-datepicker').data('datepicker').getFormattedDate('dd-mm-yyyy');
			if (date === ""){ //Sets today date if not picked
				date = moment().format("YYYY-MM-DD") + "T00:00:00Z";
			}
			else { //Or real date if picked
				date = $('#tranaction-datepicker').data('datepicker').getFormattedDate('yyyy-mm-dd') + "T00:00:00Z";
			}

			var newTransaction = {
				'date': date,
        		'value': $scope.value,
        		'currency': $scope.budget.code,
        		'type': $scope.transactionType,
        		'category': $('#new-category-field').find("option:selected").text(),
        		'name': $scope.name
			};

			api.postTransaction(newTransaction).then(
				function(data) {
					newTransaction.id = data.id;
					newTransaction.date = moment(data.created_at).format("DD-MM-YYYY");
					$scope.transactions.unshift(newTransaction); //adds transaction to view if success

					if (newTransaction.type) { // New budget value
						$scope.budget.money += newTransaction.value;
						if (newTransaction.date === $scope.today) {
							$scope.incomeToday += newTransaction.value;
						}
					}
					else{
						$scope.budget.money -= newTransaction.value;
						if (newTransaction.date === $scope.today) {
							$scope.outcomeToday -= newTransaction.value;
						}
					}
				},
				function(error) {
					console.log(error);
				}
			);
			// Close form after all
		    $("#transaction-block").slideToggle(500);
	        $("#transaction-block-show").slideToggle(500);
		}
	};


	/**
 	 * Delete transaction by on click
 	 */
	$scope.removeTransaction = function (transactionID) {
		var position = $scope.transactions.map(function(e) { return e.id; }).indexOf(transactionID);
		var transaction = $scope.transactions[position];
		api.deleteTransaction(transaction.id).then(
			function () {
				if (transaction.type) { // New budget value
					$scope.budget.money -= transaction.value;
					if (transaction.date === $scope.today) {
						$scope.incomeToday -= transaction.value;
					}
				}
				else{
					$scope.budget.money += transaction.value;
					if (transaction.date === $scope.today) {
						$scope.outcomeToday += transaction.value;
					}
				}
				//remove from view
				$scope.transactions.splice(position, 1);
				//show alert
				$("#transaction-alert").fadeIn(500);
				$("#transaction-alert").delay(2000).fadeOut(500);
				
			},  
			function (error) {
				console.log(error);
			}
		);
	};

	/**
 	 * Select transaction from list for editing
 	 */
	$scope.selectTransaction = function (index) {
		$('#edit-transaction').removeClass('disabled');
		$scope.selectedTransaction = angular.copy($scope.transactions[index]);
		if ($scope.selectedTransaction.type) {
			$("#edit-category-field").html(positiveTypes).selectpicker('refresh');
			$('#edit-outcome').addClass('active');
			$('#edit-income').removeClass('active');
			$("#edit-minus").slideUp();
			$("#edit-plus").slideDown();
		}
		else {
			$("#edit-plus").slideUp();
			$("#edit-minus").slideDown();
			$("#edit-category-field").html(negativeTypes).selectpicker('refresh');
			$('#edit-outcome').removeClass('active');
			$('#edit-income').addClass('active');
		}
		$('#edit-tranaction-datepicker').datepicker('update', new Date(dateIsoFormat($scope.selectedTransaction.date)));
		$('#edit-category-field').selectpicker('val', $scope.selectedTransaction.category);
	};


	/**
 	 * Save edited with edit_form transaction
 	 */
	$scope.editTransaction = function (edit_form) {
		if(edit_form.$valid) {
			$scope.selectedTransaction.category = $('#edit-category-field').find("option:selected").text();
			var formattedDate = $('#edit-tranaction-datepicker').data('datepicker').getFormattedDate('dd-mm-yyyy');
			if (formattedDate === ""){ //Sets today date if not picked
				formattedDate = moment().format("DD-MM-YYYY");
			}
			var isoDate = dateIsoFormat(formattedDate);
			$scope.selectedTransaction.date = isoDate;

			var position = $scope.transactions.map(function(e) { return e.id; }).indexOf($scope.selectedTransaction.id);

			api.putTransaction($scope.selectedTransaction).then(
				function () {
					$scope.selectedTransaction.date = formattedDate;
					$scope.transactions[position] = angular.copy($scope.selectedTransaction);
					getBudget();
					$scope.incomeToday = transactionsSumToday(true);
					$scope.outcomeToday = transactionsSumToday(false);
				},
				function (error) {
					console.log(error);
				}
			);
		}		
	};


	/**
 	 * Adds to value '+' sign if true - else '-' 
 	 */
	$scope.normalizeValue = function(value, type){
		if (!type){
			value = '-' + value;
		}
		else {
			value = '+' + value;
		}
		return value;
	};

	/**
 	 * Clearing inputs of new transaction form 
 	 */
	$scope.clearInputs = function() {
		$scope.name = "";
		$scope.value = "";
	};
});
