TransactionsApp.factory('TransactionsApiService', function($http, $q) {
    var service = {};

    /**
     * Making of GET request for current budget value
     */
    service.getCurrentBudget = function () {
        var defer = $q.defer();
        $http.get('/api/user-settings/')
            .success(function(response) {
                defer.resolve({
                    'currency_code': response.currency_code.title,
                    'money': response.budget.money.money
                });
            })
            .error(function(error) {
                defer.reject(error);
            });
        return defer.promise;
    };

    /**
     * Making of GET request for all data
     */
    service.getAllTransactions = function () {
        var defer = $q.defer();
        $http.get('/api/transactions/')
            .success(function(response) {
                defer.resolve(response);
            })
            .error(function(error) {
                defer.reject(error);
            });
        return defer.promise;
    };

    /**
     * Making of POST request with formated JSON data
     */
    service.postTransaction = function (transaction) {
        var defer = $q.defer();

        var data = { 
            "type": { 
                "title": transaction.category,
                "type": transaction.type
            }, 
            "money": { 
                "currency_code": { 
                    "title": transaction.currency
                }, 
                "money": transaction.value
            }, 
            "title": transaction.name, 
            "comment": "", 
            "type_transaction": transaction.type,
            "created_at": transaction.date
        };

        $http.post('/api/transaction/', data)
            .success(function(response) {
                defer.resolve(response);
            })
            .error(function(error) {
                defer.reject(error);
            });
        return defer.promise;
    };

    /**
     * Making of PUT request with formated JSON data
     */
    service.putTransaction = function (transaction) {
        var defer = $q.defer();

        var data = { 
            "id": transaction.id,
            "type": { 
                "title": transaction.category,
                "type": transaction.type
            }, 
            "money": { 
                "currency_code": { 
                    "title": transaction.currency 
                }, 
                "money": transaction.value
            },
            "title": transaction.name, 
            "comment": "", 
            "type_transaction": transaction.type,
            "created_at": transaction.date
        };

        $http.put('/api/transaction/', data)
            .success(function(response) {
                defer.resolve(response);
            })
            .error(function(error) {
                defer.reject(error);
            });
        return defer.promise;
    }

    /**
     * Making of DELETE request for transaction by id
     */
    service.deleteTransaction = function (transactionID) {
        var defer = $q.defer();
        var id = '?id=' + transactionID;
        $http.delete('/api/transaction/' + id)
            .success(function(response) {
                defer.resolve(response);
            })
            .error(function(error) {
                defer.reject(error);
            });
        return defer.promise;
    }

    return service;
});