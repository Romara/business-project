var TransactionsApp = angular.module('TransactionsApp', ['angularUtils.directives.dirPagination'])
							 .config(['$httpProvider', function($httpProvider){
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);

var positiveTypes = [];
var negativeTypes = [];

$(function(){


	/*datepicker*/
	var date = new Date();
	var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
	var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

	$('#tranaction-datepicker').datepicker({
	    language: 'ru',
	    todayHighlight: true,
	    endDate: 'now'
	});

	$('#edit-tranaction-datepicker').datepicker({
	    language: 'ru',
	    todayHighlight: true,
	    endDate: 'now'
	});

	$("#transaction-block-show").click(function(){
	    $('.has-error').removeClass('has-error');
	    $("#transaction-block").slideToggle(500);
	    $("#transaction-block-show").slideToggle(500);
	});

	$("#cancel-transaction").click(function(e){
		e.preventDefault();
	    $("#transaction-block").slideToggle(500);
	    $("#transaction-block-show").slideToggle(500);
	});

	function typesFilter(array, type) {
		return array.filter(function(e) {
			return e.type === type;
		}).map(function(e) {
			return '<option>' + e.title + '</option>';
		}).join("");
	}

	//Getting types from server
	$.ajax({
		url: '/api/types',
		dataType: 'json'
	})
	.done(function(data) {
		positiveTypes = typesFilter(data, true);

		negativeTypes = typesFilter(data, false);

		$("#new-category-field").html(negativeTypes).selectpicker('refresh');
		})
	.fail(function(err) {
		console.log(err);
	});


	
	/*Types picker*/
	$("#plus").hide();
	$("#income").click(function(){
		$("#plus").slideUp();
		$("#minus").slideDown();
		$("#new-category-field").html(negativeTypes).selectpicker('refresh');
	});
	$("#outcome").click(function(){
		$("#minus").slideUp();
		$("#plus").slideDown();
		$("#new-category-field").html(positiveTypes).selectpicker('refresh');
	});

	$("#edit-plus").hide();
	$("#edit-income").click(function(){
		$("#edit-plus").slideUp();
		$("#edit-minus").slideDown();
		$("#edit-category-field").html(negativeTypes).selectpicker('refresh');
	});
	$("#edit-outcome").click(function(){
		$("#edit-minus").slideUp();
		$("#edit-plus").slideDown();
		$("#edit-category-field").html(positiveTypes).selectpicker('refresh');
	});

});