// Chart with value of budget per day
var budgetStatusChart = document.getElementById('budget-status-chart').getContext('2d');
var budgetStatusDataset = [];
var budgetStatusLabels = [];
var Chart1;
$.ajax({
    url: '/api/budget-status/',
    dataType: 'json',
})
.done(function(data) {
    if (data.length === 0) {
        $('#budget-status-no-data').removeClass('none-display');
        $('#budget-status-chart').addClass('none-display');
        return;
    }
    var currency = data[data.length-1].budget.currency_code.title;
    budgetStatusDataset = data.map(function(e) {
        return e.budget.money;
    });
    budgetStatusLabels = data.map(function(e) {
        return moment(e.date).format('DD-MM-YYYY');
    });

    Chart1 = new Chart(budgetStatusChart, {
        type: 'line',
        data: {
            labels: budgetStatusLabels,
            datasets: [{
                data: budgetStatusDataset,
                lineTension: 0.2,
                pointRadius: 6,
                pointHoverRadius: 8,
                pointHoverBorderColor: 'teal',
                pointBackgroundColor: '#BBDEFB',
                borderColor: "rgba(153,255,51,0.4)",
                fill: false
            }]
        },
        options: {
            legend: {
                display: false
            },
            tooltips: {
                titleFontSize: 18,
                titleFontColor: '#BBDEFB',
                bodyFontSize: 16,
                displayColors: false,
                xPadding: 20,
                yPadding: 20,
                callbacks: {
                    label: function(tooltipItem, data) {
                        var allData = data.datasets[tooltipItem.datasetIndex].data;
                        var tooltipData = allData[tooltipItem.index];
                        return 'Бюджет: ' + tooltipData + ' у.е. '/* + currency*/;
                    }
                }
            }
        }
    });
})
.fail(function(error) {
  console.log(error);
});


// Chartswith value of transaction money for each category
var outcomeCategoryChart = document.getElementById("outcome-category-chart").getContext('2d');
var outcomeCategoryDataset = [];
var outcomeCategoryLabels = [];
var Chart2;

var incomeCategoryChart = document.getElementById("income-category-chart").getContext('2d');
var incomeCategoryDataset = [];
var incomeCategoryLabels = [];
var Chart3;

var pieChartsOptions = {
    legend: {
        display: true,
        labels: {fontSize: 16}
    },
    tooltips: {
        bodyFontSize: 16,
        xPadding: 13,
        yPadding: 13,
        callbacks: {
            label: function(tooltipItem, data) {
                var allData = data.datasets[tooltipItem.datasetIndex].data;
                var tooltipLabel = data.labels[tooltipItem.index];
                var tooltipData = allData[tooltipItem.index];
                var total = 0;
                for (var i in allData) {
                    total += allData[i];
                }
                var tooltipPercentage = Math.round((tooltipData / total) * 100);
                return ' ' + tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
            }
        }
    }
};

$.ajax({
    url: '/api/types-values/',
    dataType: 'json',
})
.done(function(data) {
    for (var i = data.outcome_categories.length - 1; i >= 0; i--) {
        outcomeCategoryDataset.unshift(data.outcome_categories[i][1]);
        outcomeCategoryLabels.unshift(data.outcome_categories[i][0]);
    }

    if (outcomeCategoryDataset.length === 0) {
        $('#outcome-category-no-data').removeClass('none-display');
        $('#outcome-category-chart').addClass('none-display');
    } else {
        Chart2 = new Chart(outcomeCategoryChart, {
            type: 'pie',
            data: {
                labels: outcomeCategoryLabels,
                datasets: [{
                    backgroundColor: randomColor({luminosity: 'light', count: outcomeCategoryDataset.length}),
                    data: outcomeCategoryDataset
                }]
            },
            options: pieChartsOptions
        });
    }

    for (var j = data.income_categories.length - 1; j >= 0; j--) {
        incomeCategoryDataset.unshift(data.income_categories[j][1]);
        incomeCategoryLabels.unshift(data.income_categories[j][0]);
    }

    if (incomeCategoryDataset.length === 0) {
        $('#income-category-no-data').removeClass('none-display');
        $('#income-category-chart').addClass('none-display');
    } else {
        Chart3 = new Chart(incomeCategoryChart, {
            type: 'pie',
            data: {
                labels: incomeCategoryLabels,
                datasets: [{
                    backgroundColor: randomColor({luminosity: 'light', count: incomeCategoryDataset.length}),
                    data: incomeCategoryDataset
                }]
            },
            options: pieChartsOptions
        });
    }
})
.fail(function(error) {
  console.log(error);
});

// Chart of planning
var PlanningChart = document.getElementById('planning-chart').getContext('2d');
var realDataset = [];
var planDataset = [];
var planLabels = [];
var Chart4;
$.when( $.ajax("/api/types-values/"), $.ajax("/api/safe-money/") )
    .done(function(first_call, second_call){
        var real_data = first_call[0].outcome_categories;
        var plan_data = second_call[0];

        if (plan_data.length === 0 || real_data.length === 0) {
            $('#planning-chart-no-data').removeClass('none-display');
            $('#planning-chart').addClass('none-display');
            return;
        }

        plan_data = plan_data.map(function(e) {
            return {
                'category': e.type.title,
                'value': e.money.money
            };
        });

        var chartDataRaw = [];

        var planCategories = plan_data.map(function(e) { return e.category; });
        real_data.forEach(function (e) {
            var index = planCategories.indexOf(e[0]);
            if (index > -1) {
                plan_data[index].real_value = e[1];
                chartDataRaw.push(plan_data[index]);
            }
        });
        
        for (var i = chartDataRaw.length - 1; i >= 0; i--) {
            realDataset.push(chartDataRaw[i].real_value);
            planDataset.push(chartDataRaw[i].value);
            planLabels.push(chartDataRaw[i].category);
        }

        if (planLabels.length === 0 || realDataset.length === 0) {
            $('#planning-chart-no-data').removeClass('none-display');
            $('#planning-chart').addClass('none-display');
            return;
        }

        var Chart4 = new Chart(PlanningChart, {
            type: 'horizontalBar',
            data: {
                labels: planLabels,
                datasets: [{
                    label: 'Запланировано',
                    data: planDataset,
                    backgroundColor: "rgba(255,224,230,6.0)",
                    borderColor: 'rgba(247, 143, 165, 1.0)',
                    borderWidth: 1
                },{
                    label: 'Потрачено',
                    data: realDataset,
                    backgroundColor: "rgba(215,236,251,1.0)",
                    borderColor: 'rgba(134, 216, 239, 1.0)',
                    borderWidth: 1
                }]
            },
            options: {
                legend: {
                    display: true,
                    labels: {fontSize: 16}
                },
                tooltips: {
                    titleFontSize: 18,
                    titleFontColor: '#BBDEFB',
                    bodyFontSize: 16,
                    displayColors: false,
                    xPadding: 20,
                    yPadding: 20
                }
            }
        });
    })
    .fail(function(e1, e2){
        console.log(e1, e2);
    });