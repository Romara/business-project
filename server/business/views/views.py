from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from business.models import *
from datetime import datetime


def index(request):
    if request.user.is_authenticated():
        return redirect('/finances')
    return render(request, 'business/index.html')


@login_required(login_url='/')
def transactions(request):
    return render(request, 'business/transactions.html')


@login_required(login_url='/')
def planning(request):
    return render(request, 'business/planning.html')


@login_required(login_url='/')
def settings(request):
    error = ""
    if request.method == 'POST':
        if 'currency_sub' in request.POST:
            code = CurrencyCodes.objects.get(title=request.POST['currency'])

            UserSettings.objects.filter(user=request.user).update(
                currency_code=code
            )
        elif 'category_sub' in request.POST:
            outcome = request.POST.get('outcome_category', 0)
            # income = request.POST.get('income_category', 0)
            type = bool(outcome)
            try:
                category = Type(user=request.user, title=request.POST['category'], type=type)
                category.save()
                error = "Категория добавлена."
            except Exception as e:
                error = "Такая категория уже существует."
        elif 'category_in_del_sub' in request.POST:
            try:
                category = Type.objects.get(user=request.user, title=request.POST['category_in_del'], type=True)
                category.delete()
            except Exception as e:
                pass
        elif 'category_out_del_sub' in request.POST:
            try:
                category = Type.objects.get(user=request.user, title=request.POST['category_out_del'], type=False)
                category.delete()
            except Exception as e:
                pass

    currency = UserSettings.objects.get(user=request.user).currency_code.title
    categories_outcome = Type.objects.filter(user=request.user, type=False)
    categories_income = Type.objects.filter(user=request.user, type=True)
    return render(request, 'business/settings.html', {
        'categories_outcome': categories_outcome,
        'categories_income': categories_income,
        'error': error,
        'currency': currency
    })


@login_required(login_url='/')
def finances(request):
    user = request.user

    def _transactions_sum_month(transaction_type):
        """
        Getting of sum of income/outcome transactions for current month
        """
        today = datetime.today()

        return sum(map(lambda x: x.money.money, Transaction.objects.filter(
            user=user,
            type_transaction=transaction_type,
            created_at__year=today.year,
            created_at__month=today.month
        )))

    def _average_per_day(transaction_type):
        """
        Getting of average income/outcome in day
        """
        transactions = list(map(lambda x: (x.created_at, x.money.money), Transaction.objects.filter(
            user=user,
            type_transaction=transaction_type,
        )))
        if len(transactions) == 0:
            return 0
        first_transaction_date = min([x[0] for x in transactions])
        days = abs((first_transaction_date.replace(tzinfo=None) - datetime.today()).days)
        all_transactions_sum = sum([x[1] for x in transactions])
        if days == 0:
            days = 1
        return all_transactions_sum / days

    currency = UserSettings.objects.get(user=user).currency_code.title

    budget_obj = Budget.objects.get(owner=user)
    budget = round(budget_obj.money.money, 3)
    if budget == -0:
        budget = 0
    outcome_sum = round(_transactions_sum_month(False), 3)
    income_sum = round(_transactions_sum_month(True), 3)
    total = round(income_sum - outcome_sum, 3)
    average_outcome_in_day = round(_average_per_day(False), 3)
    average_income_in_day = round(_average_per_day(True), 3)

    result = {
        'budget': budget,
        'currency': currency,
        'outcome_sum': -outcome_sum,
        'income_sum': income_sum,
        'total': total,
        'average_outcome_in_day': average_outcome_in_day,
        'average_income_in_day': average_income_in_day
    }

    return render(request, 'business/finances.html', result)
