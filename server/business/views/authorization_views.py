from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from business.models import *


def register_view(request):
    if request.method == 'POST':
        response = {}
        # Main information about user
        email = request.POST.get('email')
        password = request.POST.get('password1')
        password_repeat = request.POST.get('password2')
        # Check password
        if not check_password(password, password_repeat):
            return {'error': 'Passwords do not match'}, 400
        if User.objects.filter(username=email).exists():
            return {'error': 'User with this email already exist'}, 400
        # Additional info
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        # Create user model
        user = User(username=email, email=email)
        user.set_password(password)
        user.save()
        # Create user_settings model
        user_settings = UserSettings()
        user_settings.user = user
        user_settings.first_name = first_name
        user_settings.last_name = last_name
        money = Money()
        money.save('USD')
        budget = Budget()
        budget.owner = user
        budget.money = money
        budget.save()
        user_settings.currency_code = CurrencyCodes.objects.get(title='USD')
        user_settings.budget = budget
        user_settings.save()
        # Login after registration
        user = authenticate(username=email, password=password)
        Type.objects.create(user=user, title='Алименты', type=True).save()
        Type.objects.create(user=user, title='Арендная плата', type=True).save()
        Type.objects.create(user=user, title='Другое', type=True).save()
        Type.objects.create(user=user, title='Зарплата', type=True).save()
        Type.objects.create(user=user, title='Инвестиции', type=True).save()
        Type.objects.create(user=user, title='Соц. обеспечение', type=True).save()
        Type.objects.create(user=user, title='Автомобиль', type=False).save()
        Type.objects.create(user=user, title='Досуг', type=False).save()
        Type.objects.create(user=user, title='Жилище', type=False).save()
        Type.objects.create(user=user, title='Здоровье', type=False).save()
        Type.objects.create(user=user, title='Одежда и обувь', type=False).save()
        Type.objects.create(user=user, title='Питание', type=False).save()
        Type.objects.create(user=user, title='Счета', type=False).save()
        Type.objects.create(user=user, title='Другое', type=False).save()

        login(request, user)
        return redirect('/transactions')


def check_password(password, password_repeat):
    if len(password) < 6 | len(password) > 20:
        return False
    if password != password_repeat:
        return False
    return True


def login_view(request):
    if request.method == 'POST':
        username = request.POST.get('email')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            # the password verified for the user
            if user.is_active:
                login(request, user)
                return redirect('/')
            else:
                # The password is valid, but the account has been disabled!
                pass
        else:
            return redirect('/#login')


def email_validator(request):
    if request.method == 'GET':
        username = request.GET.get('email')
        if User.objects.filter(username=username).exists():
            return HttpResponse(status=400)
        else:
            return HttpResponse(status=200)


@login_required
def logout_view(request):
    logout(request)
    return redirect('/')
