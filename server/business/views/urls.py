# Django urls
from django.conf.urls import url
from . import views
from . import authorization_views

urlpatterns = [
    # Registration url
    url(r'^register/', authorization_views.register_view),
    url(r'^login/', authorization_views.login_view),
    url(r'^email-validator/', authorization_views.email_validator),
    url(r'^logout/', authorization_views.logout_view),
    # Main urls
    url(r'^$', views.index),
    url(r'^finances/', views.finances),
    url(r'^transactions/', views.transactions),
    url(r'^planning/', views.planning),
    url(r'^settings/', views.settings),
]
