# Django urls
from django.conf.urls import url, include


urlpatterns = [
    # API urls
    url(r'^api/', include('business.api.urls')),
    # site urls
    url(r'', include('business.views.urls')),
]
