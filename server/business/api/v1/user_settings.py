# Restful API
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
# login_required wrapper for views
from business.api.wrapper import login_required
from business.models import UserSettings
# Serializer for transaction
from business.api.serializers import UserSettingsSerializer


class UserSettingsAPI(APIView):
    @login_required
    def get(self, request):
        user_settings = UserSettings.objects.get(user=request.user)
        serializer = UserSettingsSerializer(user_settings)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request):
        user_settings = UserSettings.objects.get(user=request.user)
        serializer = UserSettingsSerializer(data=request.data)
        if serializer.is_valid():
            current_settings = serializer.update(user_settings, serializer.data)
            current_settings.save()
            return Response({'status': 'changed successful'}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
