# Restful API
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
# login_required wrapper for views
from business.api.wrapper import login_required
from business.models import Type
# Serializer for transaction
from business.api.serializers import TransactionSerializer, TypeSerializer


class TypeAPI(APIView):
    @login_required
    def post(self, request):
        request.data['user_id'] = request.user.id
        serializer = TypeSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @login_required
    def delete(self, request):
        query_params = request.query_params.dict()
        type_title = query_params.pop('title', 0)
        try:
            type_object = Type.objects.get(title=type_title, user=request.user)
        except Type.DoesNotExist:
            return Response({'error': 'type does not exist'}, status=status.HTTP_404_NOT_FOUND)
        type_object.delete()
        return Response({'status': 'deleted successful'}, status=status.HTTP_204_NO_CONTENT)


class TypesAPI(APIView):
    @login_required
    def get(self, request):
        types = Type.objects.filter(user=request.user)
        serializer = TypeSerializer(types, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @login_required
    def delete(self, request):
        types = Type.objects.filter(user=request.user)
        types.delete()
        return Response({'status': 'deleted successful'}, status=status.HTTP_204_NO_CONTENT)
