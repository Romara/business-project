# Django authorize
from django.contrib.auth import authenticate, login, logout
# Restful API
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class TestAPI(APIView):
    def get(self, request):
        return Response({'status': 'you called get method'}, status=status.HTTP_200_OK)

    def post(self, request):
        """Send login and password in json format

        Add to all requests ?format=json вроде так

        Headers need to set:
        1. Content-Type:"application/json"
        2. For login_required views also attach auth-cookie

        Your request body:
            {
                "login": "userlogin",
                "password": "user_password"
            }

        Response may be
        1. Status - You can show this message to user or not. Just additional info for sure.
        2. error - Show this message to user. It's important info about what user did wrong

        Example of response:
            {
                "error": "incorrect username or password"
            }
        """
        return Response(request.data, status=status.HTTP_200_OK)

    def put(self, requset):
        return Response({'status': 'You called method put'}, status=status.HTTP_200_OK)

    def delete(self, request):
        # 403 for testing
        return Response({'status': 'You called method delete'}, status=status.HTTP_403_FORBIDDEN)
