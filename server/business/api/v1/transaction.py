# Restful API
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
# login_required wrapper for views
from business.api.wrapper import login_required
from business.models import Transaction
# Serializer for transaction
from business.api.serializers import TransactionSerializer


class TransactionAPI(APIView):
    @login_required
    def get(self, request):
        query_params = request.query_params.dict()
        transaction_id = query_params.pop('id', 0)
        try:
            transaction = Transaction.objects.get(id=transaction_id, user=request.user)
        except Transaction.DoesNotExist:
            return Response({'error': 'wrong id or user'}, status=status.HTTP_404_NOT_FOUND)

        serializer = TransactionSerializer(transaction)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @login_required
    def post(self, request):
        request.data['user_id'] = request.user.id
        serializer = TransactionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @login_required
    def put(self, request):
        request.data['user_id'] = request.user.id
        try:
            transaction = Transaction.objects.get(pk=request.data['id'], user=request.user)
        except Transaction.DoesNotExist:
            return Response({'error': 'event does not exist'}, status=status.HTTP_404_NOT_FOUND)
        serializer = TransactionSerializer(data=request.data)
        if serializer.is_valid():
            transaction = serializer.update(transaction, serializer.data)
            transaction.save()
            serializer = TransactionSerializer(transaction)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @login_required
    def delete(self, request):
        query_params = request.query_params.dict()
        transaction_id = query_params.pop('id', 0)
        try:
            transaction = Transaction.objects.get(id=transaction_id, user=request.user)
        except Transaction.DoesNotExist:
            return Response({'error': 'transaction does not exist'}, status=status.HTTP_404_NOT_FOUND)
        transaction.delete()
        return Response({'status': 'deleted successful'}, status=status.HTTP_204_NO_CONTENT)


class TransactionsAPI(APIView):
    @login_required
    def get(self, request):
        if not request.query_params:
            """Get all transactions"""
            transactions = Transaction.objects.filter(user=request.user)
            serializer = TransactionSerializer(transactions, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)

        query_params = request.query_params.dict()

        begin_date = query_params.pop('begin_date', None)
        end_date = query_params.pop('end_date', None)

        page = query_params.pop('page', 0)
        per_page = query_params.pop('per-page', 0)

        transactions = Transaction.objects.all().filter(user=request.user)

        if begin_date:
            transactions = transactions.filter(created_at__gte=begin_date)

        if end_date:
            transactions = transactions.filter(created_at__lte=end_date)

        if not per_page:
            serializer = TransactionSerializer(transactions, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)

        serializer = TransactionSerializer(transactions[page*per_page:(page+1)*per_page], many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @login_required
    def delete(self, request):
        transactions = Transaction.objects.filter(user=request.user)
        transactions.delete()
        return Response({'status': 'deleted successful'}, status=status.HTTP_204_NO_CONTENT)
