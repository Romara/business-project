# Restful API
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
# login_required wrapper for views
from business.api.wrapper import login_required
from business.models import SafeMoney
# Serializer for transaction
from business.api.serializers import SafeMoneySerializer


class SafeMoneyAPI(APIView):
    @login_required
    def get(self, request):
        safe_money = SafeMoney.objects.filter(user=request.user)
        serializer = SafeMoneySerializer(safe_money, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @login_required
    def post(self, request):
        request.data['user_id'] = request.user.id
        serializer = SafeMoneySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @login_required
    def put(self, request):
        request.data['user_id'] = request.user.id
        try:
            safe_money = SafeMoney.objects.get(pk=request.data['id'], user=request.user)
        except SafeMoney.DoesNotExist:
            return Response({'error': 'event does not exist'}, status=status.HTTP_404_NOT_FOUND)
        serializer = SafeMoneySerializer(data=request.data)
        if serializer.is_valid():
            safe_money = serializer.update(safe_money, serializer.data)
            safe_money.save()
            serializer = SafeMoneySerializer(safe_money)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @login_required
    def delete(self, request):
        query_params = request.query_params.dict()
        transaction_id = query_params.pop('id', 0)
        try:
            transaction = SafeMoney.objects.get(id=transaction_id, user=request.user)
        except SafeMoney.DoesNotExist:
            return Response({'error': 'transaction does not exist'}, status=status.HTTP_404_NOT_FOUND)
        transaction.delete()
        return Response({'status': 'deleted successful'}, status=status.HTTP_204_NO_CONTENT)
