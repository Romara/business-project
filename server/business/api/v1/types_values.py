# Restful API
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
# login_required wrapper for views
from business.api.wrapper import login_required
from business.models import *
# DateTime
from datetime import datetime


class TypesValuesAPI(APIView):
    @login_required
    def get(self, request):

        def sum_by_category(type):
            today = datetime.today()
            transactions = Transaction.objects.filter(
                user=request.user,
                type_transaction=type,
                created_at__year=today.year,
                created_at__month=today.month
            )
            category_values = {}
            for item in transactions:
                transaction_title = item.type.title
                transaction_value = item.money.money
                if category_values.setdefault(transaction_title, []):
                    category_values[transaction_title].append(transaction_value)
                else:
                    category_values[transaction_title] = [transaction_value]
            category_sum = []
            for key in category_values:
                category_sum.append([key, sum(category_values[key])])
            return category_sum

        result = {
            'outcome_categories': sum_by_category(False),
            'income_categories': sum_by_category(True)
        }

        return Response(result, status=status.HTTP_200_OK)
