# Restful API
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
# login_required wrapper for views
from business.api.wrapper import login_required
from business.models import BudgetStatus
# Serializer for transaction
from business.api.serializers import BudgetStatusSerializer


class BudgetStatusAPI(APIView):
    @login_required
    def get(self, request):
        budget_statuses = BudgetStatus.objects.all().filter(user=request.user).order_by('date')[:30]
        serializer = BudgetStatusSerializer(budget_statuses, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
