# Django authorize
from django.contrib.auth import authenticate, login, logout
# Restful API
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
# login_required wrapper for views
from business.api.wrapper import login_required
# Serializer for login
from business.api.serializers import LoginSerializer, UserSerializer


class ProfileAPI(APIView):
    @login_required
    def get(self, request):
        """Just get method"""
        return Response({'username': request.user.username}, status=status.HTTP_200_OK)


class AuthorizeAPI(APIView):
    @login_required
    def get(self, request):
        """Get method only for check authorization"""
        return Response({'username': request.user.username}, status=status.HTTP_200_OK)

    def post(self, request):
        """Send login and password in json format

        Headers need to set:
        1. Content-Type:"application/json"
        2. For login_required views also attach auth-cookie

        Your request body:
            {
                "login": "userlogin",
                "password": "user_password"
            }

        Response may be
        1. Status - You can show this message to user or not. Just additional info for sure.
        2. error - Show this message to user. It's important info about what user did wrong

        Example of response:
            {
                "error": "incorrect username or password"
            }
        """
        serializer = LoginSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        user = authenticate(**serializer.data)
        if user is not None:
            if user.is_active:
                login(request, user)
                return Response({'status': 'login successful'}, status=status.HTTP_200_OK)
            else:
                return Response({'status': 'user is inactive'}, status=status.HTTP_204_NO_CONTENT)
        else:
            return Response({'error': 'incorrect username or password'}, status=status.HTTP_400_BAD_REQUEST)

    @login_required
    def delete(self, request):
        """This view logout user"""
        logout(request)
        return Response({'status': 'logout success'}, status=status.HTTP_200_OK)


class RegisterAPI(APIView):
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
