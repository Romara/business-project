from rest_framework.response import Response
from rest_framework import status


def login_required(func):
    def check_login(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return Response({'error': 'user is not authenticated'}, status=status.HTTP_401_UNAUTHORIZED)
        return func(self, request, *args, **kwargs)
    return check_login
