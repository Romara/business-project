# Django user model
from django.contrib.auth.models import User
# Serializer class
from rest_framework import serializers
# Models
from business.models import Transaction, Money, Type, CurrencyCodes, UserSettings, Budget, BudgetStatus, SafeMoney
# Support libs
import copy


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=25)
    password = serializers.CharField(max_length=25)

    def update(self, instance, validated_data):
        instance.username = validated_data.get('username', instance.username)
        instance.password = validated_data.get('password', instance.password)
        return instance

    def create(self, validated_data):
        # We do not save this serializer
        pass


class UserSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        user = User(username=validated_data['username'], email=validated_data['email'])
        user.set_password(validated_data['password'])
        return user

    class Meta:
        model = Type


class CurrencyCodesSerializer(serializers.ModelSerializer):
    title = serializers.CharField(max_length=4)

    def create(self, validated_data):
        currency_code = CurrencyCodes.objects.get(title=validated_data['title'])
        return currency_code

    class Meta:
        model = CurrencyCodes
        fields = ('title',)


class MoneySerializer(serializers.ModelSerializer):
    currency_code = CurrencyCodesSerializer()

    class Meta:
        model = Money
        exclude = ('id',)


class TypeSerializer(serializers.ModelSerializer):
    title = serializers.CharField(max_length=255)

    class Meta:
        model = Type
        exclude = ('user',)


class TransactionSerializer(serializers.ModelSerializer):
    type = TypeSerializer()
    money = MoneySerializer()
    user_id = serializers.IntegerField()

    def create(self, validated_data):
        transaction_type = Type.objects.get(title=validated_data['type']['title'], user_id=validated_data['user_id'], type=validated_data['type']['type'])
        data = copy.deepcopy(validated_data)
        data.pop('money')
        data.pop('type')
        transaction = Transaction(type=transaction_type, **data)
        transaction.save(money_count=validated_data['money']['money'],
                         currency_code_title=validated_data['money']['currency_code']['title'])
        return transaction

    def update(self, instance, validated_data):
        budget_money = Budget.objects.get(owner=instance.user).money

        if instance.type_transaction:
            budget_money -= instance.money
        else:
            budget_money += instance.money

        budget_money.save()

        instance.comment = validated_data.get('comment')
        instance.title = validated_data.get('title')
        instance.created_at = validated_data.get('created_at')
        instance.type_transaction = validated_data.get('type_transaction')

        instance.type = Type.objects.get(title=validated_data.get('type').get('title'),
                                         type=validated_data.get('type').get('type'),
                                         user_id=validated_data.get('user_id'))

        instance.money.money = validated_data.get('money').get('money')
        instance.money.save(currency_code_title=validated_data.get('money').get('currency_code').get('title'))

        return instance

    class Meta:
        model = Transaction
        exclude = ('user',)


class BudgetSerializer(serializers.ModelSerializer):
    money = MoneySerializer()

    class Meta:
        model = Budget
        fields = ('money',)


class UserSettingsSerializer(serializers.ModelSerializer):
    currency_code = CurrencyCodesSerializer()
    budget = BudgetSerializer()

    class Meta:
        model = UserSettings
        exclude = ('id', 'user')


class BudgetStatusSerializer(serializers.ModelSerializer):
    budget = MoneySerializer()

    class Meta:
        model = BudgetStatus
        exclude = ('id', 'user')


class SafeMoneySerializer(serializers.ModelSerializer):
    money = MoneySerializer()
    type = TypeSerializer()
    user_id = serializers.IntegerField()

    def create(self, validated_data):
        data = copy.deepcopy(validated_data)

        type = Type.objects.get(title=validated_data.get('type').get('title'),
                                         type=validated_data.get('type').get('type'),
                                         user_id=validated_data.get('user_id'))

        money_info = data.pop('money')
        
        money = Money()
        money.money = money_info['money']
        money.save(currency_code_title=money_info['currency_code']['title'])

        type_info = data.pop('type')

        safe_money = SafeMoney(**data)
        safe_money.money = money
        safe_money.type = type
        safe_money.save()
        return safe_money

    def update(self, instance, validated_data):
        instance.comment = validated_data.get('comment')

        instance.type = Type.objects.get(title=validated_data.get('type').get('title'),
                                         type=validated_data.get('type').get('type'),
                                         user_id=validated_data.get('user_id'))

        instance.money.money = validated_data.get('money').get('money')
        instance.money.save(currency_code_title=validated_data.get('money').get('currency_code').get('title'))
        instance.save()
        return instance

    class Meta:
        model = SafeMoney
        exclude = ('user',)
