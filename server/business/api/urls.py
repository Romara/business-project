# Django urls
from django.conf.urls import url
# My Auth API
from business.api.v1.budget_status import BudgetStatusAPI
from business.api.v1.types_values import TypesValuesAPI
from business.api.v1.authorize import ProfileAPI, RegisterAPI, AuthorizeAPI
from business.api.v1.transaction import TransactionAPI, TransactionsAPI
from business.api.v1.user_settings import UserSettingsAPI
from business.api.v1.type import TypeAPI, TypesAPI
from business.api.v1.safe_money import SafeMoneyAPI
# Test API
from business.api.v1.test_views import TestAPI


urlpatterns = [
    url(r'^authorize/$', AuthorizeAPI.as_view()),
    url(r'^profile/$', ProfileAPI.as_view()),
    url(r'^user-settings/$', UserSettingsAPI.as_view()),
    url(r'^register/$', RegisterAPI.as_view()),
    url(r'^transaction/$', TransactionAPI.as_view()),
    url(r'^transactions/$', TransactionsAPI.as_view()),
    url(r'^type/$', TypeAPI.as_view()),
    url(r'^types/$', TypesAPI.as_view()),
    url(r'^budget-status/$', BudgetStatusAPI.as_view()),
    url(r'^types-values/$', TypesValuesAPI.as_view()),
    url(r'^safe-money/$', SafeMoneyAPI.as_view()),
    url(r'^test/$', TestAPI.as_view()),
]
