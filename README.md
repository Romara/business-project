# README #

Install vagrant (use virtualbox)

* Clone repository

```
#!python

git clone https://Romara@bitbucket.org/Romara/business-project.git <folder>
```

* GoTo project folder

```
#!python

cd <folder> 
```

* Create machine
```
#!python

vagrant up
```

* Just run this command, for sure.
```
#!python

vagrant provision
```

* Enter
```
#!python

vagrant ssh
```

* Enter virtualenv with python3 as default
```
#!python

workon project
```

* Cd to server
```
#!python

cd /vagrant/server
```

* Create DB
```
#!python

python manage.py migrate
```

* Run server
```
#!python

python manage.py runserver 0.0.0.0:8081
```
PROFIT!!